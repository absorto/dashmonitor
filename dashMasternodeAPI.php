<?php
/**
 * Simple API for obtaining information about DASH masternodes
 * Gets payment details and status of masternodes associated with the specified payee address
 * - The API depends on a local dash daemon and dash-cli in PATH
 */

header( 'Content-Type: application/json; charset=UTF-8' );

// Assign payee address or error if not provided
$payee = $_REQUEST['payee'] ?? exit( '{"error":"Missing payee parameter."}' );

// Scan masternodes
$nodes = dashCli( 'masternode list' );
$out = [];

foreach ( $nodes as $node ) {

	// Process node if it is associated to payout address 
	if ( $node->payee == $payee ) {

		// Get full info for the payout block, and 1st tx in the block
		$blockHash = dashCli( 'getblockhash ' . $node->lastpaidblock );
		$blockInfo = dashCli( 'getblock ' . $blockHash );
		$txId = $blockInfo->tx[0];
		$txInfo = dashCli( 'getrawtransaction ' . $txId . ' 1' );

		// Add necessary info to the output array
		$out[] = [
			'status'            => $node->status ?: 'UNKNOWN',
			'lastPaidBlock'     => $node->lastpaidblock,
			'lastPaidTime'      => $node->lastpaidtime,
			'collateralAddress' => $node->collateraladdress,
			'amount'            => $txInfo->vout[0]->value,
		];
	}
}

// Report info found or error if nothing
$out ?: exit( '{"error":"No masternodes found matching this payee address."}' );
print json_encode( $out );

/**
 *  Send a command to the local dash daemon and encode if it's JSON
 */
function dashCli( $cmd ) {
	$output = trim( `dash-cli $cmd` );
	if ( preg_match( '/^\{/', $output ) ) $output = json_decode( $output );
	return $output;
}
